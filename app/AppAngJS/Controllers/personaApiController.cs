﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using layerBuss;
using System.Data;
namespace AppAngJS.Controllers
{
    public class personaApiController : ApiController
    {
        public HttpResponseMessage Get()
        {
            List<clsPersona> listaper = new List<clsPersona>();
            foreach (DataRow dr in clsPersona.listarPersona().Rows)
            {
                clsPersona per = new clsPersona();
                per.IdPersona = Convert.ToInt32(dr["IdPersona"]);
                per.IdTipoDocumento = Convert.ToByte(dr["IdTipoDocumento"]);
                per.TipoPersona = Convert.ToByte(dr["TipoPersona"]);
                per.Nombres1 = Convert.ToString(dr["Nombres1"]);
                per.Nombres2 = Convert.ToString(dr["Nombres2"]);
                per.ApePaternoRazSoc = Convert.ToString(dr["ApePaternoRazSoc"]);
                per.ApeMaterno = Convert.ToString(dr["ApeMaterno"]);
                per.DNIRUC = Convert.ToString(dr["DNIRUC"]);
                per.FNacimiento = Convert.ToDateTime(dr["FNacimiento"]);
                per.Sexo = Convert.ToString(dr["Sexo"]);
                per.Direccion = Convert.ToString(dr["Direccion"]);
                per.Numero = Convert.ToString(dr["Numero"]);
                per.OBS = Convert.ToString(dr["OBS"]);
                listaper.Add(per);
            }
            return Request.CreateResponse<List<clsPersona>>(HttpStatusCode.OK,listaper);        
        }
        public HttpResponseMessage Post([FromBody] clsPersona nuevaPersona)
        {
            DataTable DTR = new DataTable();
            DTR=clsPersona.registrarPersona(nuevaPersona.IdTipoDocumento, nuevaPersona.TipoPersona, nuevaPersona.Nombres1, nuevaPersona.Nombres2, nuevaPersona.ApePaternoRazSoc, nuevaPersona.ApeMaterno, nuevaPersona.DNIRUC, nuevaPersona.FNacimiento, nuevaPersona.Sexo, nuevaPersona.Direccion, nuevaPersona.Numero, nuevaPersona.OBS);
            List<respuesta> lrsp = new List<respuesta>();
            respuesta rsp = new respuesta();
            rsp.iderror = Convert.ToInt32(DTR.Rows[0]["iderror"]);
            rsp.msg = DTR.Rows[0]["msg"].ToString();
            lrsp.Add(rsp);
                                   
            //var msg = new HttpResponseMessage(HttpStatusCode.Created);
            //msg = DTR.Rows[0]["msg"].ToString();
            //msg.Headers.Location = new Uri(Request.RequestUri+nuevaPersona.IdPersona.ToString());
             return Request.CreateResponse<List<respuesta>>(HttpStatusCode.OK, lrsp);
            //return msg;
        }

        public HttpResponseMessage Put([FromBody] clsPersona Persona)
        {
            DataTable DTR = new DataTable();
            DTR = clsPersona.actualizarPersona( Persona.IdPersona,Persona.IdTipoDocumento, Persona.TipoPersona, Persona.Nombres1, Persona.Nombres2, Persona.ApePaternoRazSoc, Persona.ApeMaterno, Persona.DNIRUC, Persona.FNacimiento, Persona.Sexo, Persona.Direccion, Persona.Numero, Persona.OBS);
            List<respuesta> lrsp = new List<respuesta>();
            respuesta rsp = new respuesta();
            rsp.iderror = Convert.ToInt32(DTR.Rows[0]["iderror"]);
            rsp.msg = DTR.Rows[0]["msg"].ToString();
            lrsp.Add(rsp);

            //var msg = new HttpResponseMessage(HttpStatusCode.Created);
            //msg = DTR.Rows[0]["msg"].ToString();
            //msg.Headers.Location = new Uri(Request.RequestUri+nuevaPersona.IdPersona.ToString());
            return Request.CreateResponse<List<respuesta>>(HttpStatusCode.OK, lrsp);
            //return msg;
        }
        public HttpResponseMessage Delete(int id)
        {
            DataTable DTR = new DataTable();
            DTR = clsPersona.borrarPersona(id);
            List<respuesta> lrsp = new List<respuesta>();
            respuesta rsp = new respuesta();
            rsp.iderror = Convert.ToInt32(DTR.Rows[0]["iderror"]);
            rsp.msg = DTR.Rows[0]["msg"].ToString();
            lrsp.Add(rsp);

            //var msg = new HttpResponseMessage(HttpStatusCode.Created);
            //msg = DTR.Rows[0]["msg"].ToString();
            //msg.Headers.Location = new Uri(Request.RequestUri+nuevaPersona.IdPersona.ToString());
            return Request.CreateResponse<List<respuesta>>(HttpStatusCode.OK, lrsp);
            //return msg;
        }
    }
    public class respuesta {
        public int iderror { get; set; }
        public string msg { get; set; }
    }

}
