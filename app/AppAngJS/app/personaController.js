﻿app.controller("personaController",function($scope,personaRepository){
    //-----------------------Valida datos persona ----------------
    var validar = function (persona) {
        if (persona.ApePaternoRazSoc == '') {
            alert("Ingrese Apellido Paterno!!!");
            return false;
        }
        if (persona.ApeMaterno == '') {
            alert("Ingrese Apellido Materno!!!");
            return false;
        }
        if (persona.Nombres1 == '') {
            alert("Ingrese Primer Nombre!!!");
            return false;
        }
        if (persona.Nombres2 == '') {
            alert("Ingrese Segundo Nombre!!!");
            return false;
        }
        if (persona.Sexo == '') {
            alert("Ingrese Genero!!!");
            return false;
        }
        return true;
    }
    //-----------------------Fin Valida datos persona ----------------
    //-----------------------Listar personas ----------------
    var getSuccessCallback = function (data, status) {
        //alert("Llamada exitosa!!!");
        $scope.listapersonas=data;
    }
    var errorCallback = function (data, status,headers,config) {
        //alert("Ocurrio un problema!!!");
        alert(data[0].msg);
    }
    personaRepository.get()
        .success(getSuccessCallback)
        .error(errorCallback);
    //-----------------------Fin Listar personas ----------------
    //-----------------------Nueva persona ----------------
    $scope.nuevaPersona = null;
    $scope.agregarNuevaPersona = function () {

        $scope.nuevaPersona = { "idPersona": 0, "ApePaternoRazSoc" :"","ApeMaterno":"","Nombres1":"","Nombres2":"","Sexo":""};
        $("#nuevaPersona").modal('show');
    }
    var postSuccessCallback = function (data, status,headers,config) {
        alert(data[0].msg);
        $("#nuevaPersona").modal('hide');

        personaRepository.get()
       .success(getSuccessCallback)
       .error(errorCallback);
        //"Persona Creada!!!" 
    }
    $scope.guardarNuevaPersona=function(nuevaPersona){
        if (validar(nuevaPersona)){        
                personaRepository.add(nuevaPersona)
                .success(postSuccessCallback)
                .error(errorCallback);
        }
    }
    //-----------------------Fin Nueva persona ----------------
    //-----------------------Edicion de persona ----------------
    $scope.editar = function (persona)
    {
        $scope.editarPersona = persona;
        $("#EditarPersona").modal('show');
    }
    $scope.guardarPersona = function (persona) {
        if (validar(persona)) {
            personaRepository.update(persona)
            .success(putSuccessCallback)
            .error(errorCallback);
        }
    }
    var putSuccessCallback = function (data, status, headers, config) {
        alert(data[0].msg);
        $("#EditarPersona").modal('hide');

        personaRepository.get()
       .success(getSuccessCallback)
       .error(errorCallback);
        //"Persona Creada!!!" 
    }
    //-----------------------Fin Edicion de persona ----------------
    //-----------------------Eliminar persona ----------------
    var deleteSuccessCallback = function (data, status, headers, config) {
        alert(data[0].msg);
        personaRepository.get()
        .success(getSuccessCallback)
        .error(errorCallback);
    }
    var deleteErrorCallback = function (data, status, headers, config) {
        alert("Ocurrio un problema!!!");
       // alert(data[0].msg);
    }
    $scope.eliminar = function (persona) {
        personaRepository.delete(persona).success(deleteSuccessCallback).error(deleteErrorCallback);
    }

    //-----------------------Fin Eliminar persona ----------------
});