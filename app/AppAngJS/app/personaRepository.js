﻿var personaUrl="/api/personaApi/";
app.factory("personaRepository", function ($http) {

    return {
        get: function() { return $http.get(personaUrl) },
        add: function(persona) { return $http.post(personaUrl, persona) },
        update: function(persona) { return $http.put(personaUrl, persona) },
        delete: function (persona) { return $http.delete(personaUrl+persona.IdPersona) }
    };

});