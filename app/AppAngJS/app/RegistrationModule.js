﻿var app=angular.module("app",["ngRoute"]).config(function($routeProvider,$locationProvider){
    $routeProvider.when("/persona",{
        templateUrl:"/app/templates/persona.html",
        controller:"personaController"
    });
    $locationProvider.html5Mode({
          enabled: true,
          requireBase: false
    });
});