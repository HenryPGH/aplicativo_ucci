﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using layerData.dsSysFactPRODTableAdapters;
using layerData;
using System.Data;

namespace layerBuss
{
    public class clsCategoria
    {
        public int idCategoria {set; get;}
        public string descripcion { set; get; } 

        public static DataTable InsertaCategoria (string descripcion) {
            sp_InsertarCategoriaTableAdapter TACategoria = new sp_InsertarCategoriaTableAdapter();
            dsSysFactPROD.sp_InsertarCategoriaDataTable DTCategoria = new dsSysFactPROD.sp_InsertarCategoriaDataTable();
            DTCategoria = TACategoria.GetData(descripcion);
            return DTCategoria;             
        }
        public static DataTable ModificaCategoria(int idCategoria,string descripcion)
        {
            sp_ActualizarCategoriaTableAdapter TACategoria = new sp_ActualizarCategoriaTableAdapter();
            dsSysFactPROD.sp_ActualizarCategoriaDataTable DTCategoria = new dsSysFactPROD.sp_ActualizarCategoriaDataTable();
            DTCategoria = TACategoria.GetData(idCategoria,descripcion);
            return DTCategoria;
        }
        public static DataTable EliminaCategoria(int idCategoria)
        {
            sp_EliminarCategoriaTableAdapter TACategoria = new sp_EliminarCategoriaTableAdapter();
            dsSysFactPROD.sp_EliminarCategoriaDataTable DTCategoria = new dsSysFactPROD.sp_EliminarCategoriaDataTable();
            DTCategoria = TACategoria.GetData(idCategoria);
            return DTCategoria;
        }
        public static DataTable BucaCategoria(int idCategoria)
        {
            sp_BuscarCategoriaTableAdapter TACategoria = new sp_BuscarCategoriaTableAdapter();
            dsSysFactPROD.sp_BuscarCategoriaDataTable DTCategoria = new dsSysFactPROD.sp_BuscarCategoriaDataTable();
            DTCategoria = TACategoria.GetData(idCategoria);
            return DTCategoria;
        }
    }
}
