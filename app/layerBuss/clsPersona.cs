﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using layerData.dssysventaTableAdapters;
using layerData;
using System.Data;


namespace layerBuss
{
    public class clsPersona
    {
        public int IdPersona { get; set; }
        public byte IdTipoDocumento { get; set; }
        public byte TipoPersona { get; set; }
        public string Nombres1 { get; set; }
        public string Nombres2 { get; set; }
        public string ApePaternoRazSoc { get; set; }
        public string ApeMaterno { get; set; }
        public string DNIRUC { get; set; }
        public DateTime FNacimiento { get; set; }
        public string Sexo { get; set; }
        public string Direccion { get; set; }
        public string Numero { get; set; }
        public string OBS { get; set; }


        public static DataTable listarPersona()
        {
            sp_listarPersonaTableAdapter TAlistap = new sp_listarPersonaTableAdapter();
            dssysventa.sp_listarPersonaDataTable DTlistap = new dssysventa.sp_listarPersonaDataTable();
            DTlistap = TAlistap.GetData();
            return DTlistap;
        }
        public static DataTable registrarPersona(byte parIdTipoDocumento, byte parTipoPersona, string parNombres1, string parNombres2, string parApePaternoRazSoc, string parApeMaterno, string parDNIRUC, DateTime parFNacimiento, string parSexo, string parDireccion, string parNumero, string parOBS)
        {
            sp_registrarPersonaTableAdapter TARegistrarPer = new sp_registrarPersonaTableAdapter();
            dssysventa.sp_registrarPersonaDataTable DTRegistrarPer = new dssysventa.sp_registrarPersonaDataTable();
            DTRegistrarPer = TARegistrarPer.GetData(parIdTipoDocumento, parTipoPersona, parNombres1, parNombres2, parApePaternoRazSoc, parApeMaterno, parDNIRUC, parFNacimiento, parSexo, parDireccion, parNumero, parOBS);
            return DTRegistrarPer;
        }
        public static DataTable actualizarPersona(int parIDPersona, byte parIdTipoDocumento, byte parTipoPersona, string parNombres1, string parNombres2, string parApePaternoRazSoc, string parApeMaterno, string parDNIRUC, DateTime parFNacimiento, string parSexo, string parDireccion, string parNumero, string parOBS)
        {
            sp_actualizarPersonaTableAdapter TAactualizarPer = new sp_actualizarPersonaTableAdapter();
            dssysventa.sp_actualizarPersonaDataTable DTactualizarPer = new dssysventa.sp_actualizarPersonaDataTable();
            DTactualizarPer = TAactualizarPer.GetData(parIDPersona, parIdTipoDocumento, parTipoPersona, parNombres1, parNombres2, parApePaternoRazSoc, parApeMaterno, parDNIRUC, parFNacimiento, parSexo, parDireccion, parNumero, parOBS);
            return DTactualizarPer;
        }
        public static DataTable borrarPersona(int parIDPersona)
        {
            sp_borrarPersonaTableAdapter TAborrarPer = new sp_borrarPersonaTableAdapter();
            dssysventa.sp_borrarPersonaDataTable DTborrarPer = new dssysventa.sp_borrarPersonaDataTable();
            DTborrarPer = TAborrarPer.GetData(parIDPersona);
            return DTborrarPer;
        }
    }
}


